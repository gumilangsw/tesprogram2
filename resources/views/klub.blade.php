@extends('layouts.app')
@section('content')
<div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
              <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                <h6 class="text-white text-capitalize ps-3">KLUB PESERTA</h6>
              </div>
            </div>
            @if ($message = Session::get('sukses'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">x</button> 
					<strong>{{ $messages }}</strong>
				</div>
				@endif
				
            <div class="card-body px-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                	<div class="col-6 text-end">
                      <a class="btn bg-gradient-dark mb-0" href="{{route('klub.create')}}"><i class="material-icons text-sm">add</i>&nbsp;&nbsp;Tambah Klub</a>
                     </div>  <br>
                		<thead>
                    	<tr>
							
                      		<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Klub</th>
                      		<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Kota</th>
                      		<th class="text-secondary opacity-7"></th>
                    	</tr>
                  		</thead>
						@foreach($klubs as $p)

                  		<tbody>
                    	<tr>
                    	<td>{{ $p->nama_klub }}</td>
                    	<td>{{ $p->kota_klub }}	
						</td>
						@endforeach
						{{ $klubs->links() }}
						</tbody>
                	</table>
              	</div>
            </div>
        </div>
    </div>
</div>

      
@push('js-source')

@endpush

@endsection
