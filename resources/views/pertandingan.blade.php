@extends('layouts.app')
@section('content')

<div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
              <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                <h6 class="text-white text-capitalize ps-3">INPUT DATA PERTANDINGAN</h6>
              </div>
            </div>
            @if ($message = Session::get('sukses'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">x</button> 
					<strong>{{ $message }}</strong>
				</div>
				@endif

      @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li> {{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif

                            <div class="card-body">
            <form action="/pertandingan/store" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}

              <div class="row">

              <div class="col-md-7 mt-4">

              <div class="card">

                  <div class="card-header pb-0 px-3">
                        <h6 class="mb-0"></h6>
                      </div>

                      <div class="input-group input-group-outline mb-3">
                        <label >Nama Klub Kandang</label></div>
                      <div class="input-group input-group-outline mb-3">
                        <select   name="nama_klub_kandang" id="nama_klub_kandang" class="form-control">
                        <option value="">SELECT KLUB</option>
                        @foreach($klubs as $p)
                        <option value="{{ $p->nama_klub }}">{{ $p->nama_klub }}</option>
                        @endforeach  
                      </select>
                      </div>
                      <h1>VS</h1>
                      <div class="input-group input-group-outline mb-3">
                        <label >Nama Klub Tandang</label></div>
                      <div class="input-group input-group-outline mb-3">
                      <select   name="nama_klub_tandang" id="nama_klub_tandang" class="form-control">
                        <option value="">SELECT KLUB</option>
                        @foreach($klubs as $p)
                        <option value="{{ $p->nama_klub }}">{{ $p->nama_klub }}</option>
                        @endforeach  
                      </select>
                      </div>
                      </div>
                      </div>
                      <div class="col-lg-4 col-md-6">
                    <div class="card h-100">
                    <div class="card-header pb-0"><br>
                    <div class="input-group input-group-outline mb-3">
                      <br>
                      <label >Skor Tim Kandang</label></div>
                      <div class="input-group input-group-outline mb-3">
                      <input type="number" class="form-control" name="skor_klub_kandang" id="skor_klub_kandang" placeholder="masukan skor" number>
                      <div class="input-group input-group-outline mb-3">
                      <br>
                      <label >id Tim Kandang</label></div>
                      
                      <input type="number" class="form-control" name="klub_kandang_id" id="klub_kandang_id" placeholder="masukan id klub Kandang" number>

                    </div>
                    <div class="input-group input-group-outline mb-3">
                      <h2>VS</h2></div>
                    
                    <div class="input-group input-group-outline mb-3">
                      <label >Skor Tim Tandang</label></div>
                      <div class="input-group input-group-outline mb-3">   
                      <input type="number" class="form-control" name="skor_klub_tandang" id="skor_klub_tandang" placeholder="masukan skor">
                      <div class="input-group input-group-outline mb-3">
                      <label >Skor Tim Tandang</label></div>
                    
                      <input type="number" class="form-control" name="klub_tandang_id" id="klub_tandang_id" placeholder="masukan ID klub tandang" number>
                      <select   name="status" id="status" class="form-control">
                        <option value="">Status pertandingan</option>
                        <option value="seri">seri</option>
                        <option value="finished">finished</option>
                          
                      </select>
                    </div>
                    <div class="text-center">
                      <input type="submit" value="Simpan Data" class="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0">
                      <div class="input-group-addon ml-3"> 
				            </div>
                    </div>
      
      
</form>
                        <br>
                    @push('js-source')

@endpush

@endsection



