@extends('layouts.app')
@section('content')

<div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
              <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                <h6 class="text-white text-capitalize ps-3">INPUT DATA KLUB</h6>
              </div>
            </div>
            @if ($message = Session::get('sukses'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">x</button> 
					<strong>{{ $message }}</strong>
				</div>
				@endif

      @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li> {{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <div class="card-body">
            <form action="/klub/store" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
              <div class="row">
                <div class="col-md-7 mt-4">
                  <div class="card">
                      <div class="card-header pb-0 px-3">
                        <h6 class="mb-0"></h6>
                      </div>
                      <div class="input-group input-group-outline mb-3">
                        <label >Nama Klub</label></div>
                      <div class="input-group input-group-outline mb-3">
                        <input type="text" class="form-control"  name="nama_klub" id="nama_klub" >
                      </div>
                      <div class="input-group input-group-outline mb-3">
                        <label >Kota Klub</label></div>
                      <div class="input-group input-group-outline mb-3">
                        <input type="text" class="form-control"  name="kota_klub" id="kota_klub" required>
                        </div>
                       
                      
                    <div class="text-center">
                      <input type="submit" value="Simpan Data" class="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0">
                  </div>
      </form>
@push('js-source')

@endpush

@endsection



