@extends('layouts.app')
@section('content')
<div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
              <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                <h6 class="text-white text-capitalize ps-3">PT. LIGA</h6>
              </div>
            </div>
            @if ($message = Session::get('sukses'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">x</button> 
					<strong>{{ $message }}</strong>
				</div>
				@endif
            <div class="card-body px-0 pb-2">
              <div class="table-responsive p-0">
              	</div>
            </div>
        </div>
    </div>
</div>

      
@push('js-source')

@endpush

@endsection
