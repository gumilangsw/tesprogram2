@extends('layouts.app')
@section('content')
<div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
              <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                <h6 class="text-white text-capitalize ps-3">Pertandingan</h6>
              </div>
            </div>
            @if ($message = Session::get('sukses'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">x</button> 
					<strong>{{ $message }}</strong>
				</div>
				@endif
            <div class="card-body px-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                	<div class="col-6 text-end">
                     </div>  <br>
                <thead>
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">no</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Klub</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Main</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Menang</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Seri</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">kalah</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">GM</th>
					            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">GK</th>
					            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">point</th>
					  

                      <th class="text-secondary opacity-7"></th>
                    </tr>
                  </thead>
                  @foreach($klasemen as $p)

<tbody>
<tr>
<td>-</td>
<td>{{ $p->nama_klub }}</td>
<td>{{ $p->main }}</td>
<td>{{ $p->menang }}</td>
<td>{{ $p->seri }}</td>
<td>{{ $p->kalah }}</td>
<td>{{ $p->golmasuk }}</td>
<td>{{ $p->golkemasukan }}</td>
<td>{{ $p->point}}</td>

@endforeach

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      
	  @push('js-source')

@endpush

@endsection
