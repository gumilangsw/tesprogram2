<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class klasemen extends Model
{
    use HasFactory;
      /**
     * fillable
     *
     * @var array
     */
    protected $fillable = [
        'nama_klub',
        'main',
        'menang',
        'seri',
        'kalah',
        'golmasuk',
        'golkemasukan',
        'point'
    ];
}
