<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pertandingan extends Model
{
    use HasFactory;
         /**
     * fillable
     *
     * @var array
     */
    protected $fillable = [
    'nama_klub_kandang',
    'klub_kandang_id',
    'nama_klub_tandang',
    'klub_tandang_id',
    'skor_klub_kandang',
    'skor_klub_tandang',
    'status'
   
    ];
}
