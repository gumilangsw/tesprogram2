<?php
 
 namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


use Session;
class KlasemenController extends Controller
{
    public function klasemen()
    {
        $klasemen = DB::table('klasemens')
        ->select('klasemens.nama_klub as nama_klub', 
             DB::raw('SUM(CASE WHEN (pertandingans.klub_kandang_id = klasemens.id OR pertandingans.klub_tandang_id = klasemens.id) AND pertandingans.status = "finished" AND klasemens.main + main THEN 1 ELSE 1 END) AS main'), 
             DB::raw('SUM(CASE WHEN pertandingans.klub_kandang_id = klasemens.id AND pertandingans.status = "finished" AND pertandingans.skor_klub_kandang > pertandingans.skor_klub_tandang AND pertandingans.klub_tandang_id = klasemens.id  THEN 1 ELSE 0 END) AS menang'), 
  
             DB::raw('SUM(CASE WHEN pertandingans.klub_kandang_id = klasemens.id AND pertandingans.status = "finished" AND pertandingans.skor_klub_kandang < pertandingans.skor_klub_tandang  THEN 1 ELSE 0 END) AS kalah'), 
             DB::raw('SUM(CASE WHEN pertandingans.klub_tandang_id = klasemens.id AND pertandingans.status = "finished" AND pertandingans.skor_klub_tandang < pertandingans.skor_klub_kandang THEN 1 ELSE 0 END) AS kalah'),
             DB::raw('SUM(CASE WHEN (pertandingans.klub_kandang_id = klasemens.id OR pertandingans.klub_tandang_id = klasemens.id) AND pertandingans.status = "seri" AND pertandingans.skor_klub_kandang = skor_klub_tandang THEN 1 ELSE 1 END) AS seri'), 
             DB::raw('SUM(CASE WHEN pertandingans.klub_kandang_id = klasemens.id AND pertandingans.status = "finished" AND pertandingans.skor_klub_kandang < pertandingans.skor_klub_tandang THEN pertandingans.skor_klub_kandang ELSE 0 END) + SUM(CASE WHEN pertandingans.klub_tandang_id = klasemens.id AND pertandingans.status = "finished" AND pertandingans.skor_klub_tandang < pertandingans.skor_klub_kandang THEN pertandingans.skor_klub_tandang ELSE 0 END) AS golmasuk'), 
             DB::raw('SUM(CASE WHEN pertandingans.klub_kandang_id = klasemens.id AND pertandingans.status = "finished" AND pertandingans.skor_klub_kandang > pertandingans.skor_klub_tandang THEN pertandingans.skor_klub_tandang ELSE 0 END) + SUM(CASE WHEN pertandingans.klub_tandang_id = klasemens.id AND pertandingans.status = "finished" AND pertandingans.skor_klub_tandang > pertandingans.skor_klub_kandang THEN pertandingans.skor_klub_kandang ELSE 0 END) AS golkemasukan'), 
             DB::raw('SUM(CASE WHEN pertandingans.klub_kandang_id = klasemens.id AND pertandingans.status = "finished" AND pertandingans.skor_klub_kandang > pertandingans.skor_klub_tandang THEN 3 WHEN pertandingans.klub_tandang_id = klasemens.id AND pertandingans.status = "finished" AND pertandingans.skor_klub_tandang > pertandingans.skor_klub_kandang THEN 3 ELSE 0 END) AS point'))
    ->leftJoin('pertandingans', function($join) {
        $join->on('klasemens.id', '=', 'pertandingans.klub_kandang_id')
             ->orWhere('klasemens.id', '=', 'pertandingans.klub_tandang_id');
    })
    ->groupBy('klasemens.nama_klub')
    ->orderByDesc('point')
    ->orderByDesc(DB::raw('golmasuk - golkemasukan'))
    ->get();
// dd($klasemen);
    return view('klasemen', ['klasemen' => $klasemen]);
    }
}