<?php
 
 namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

use Session;
class KlubController extends Controller
{
    public function klub()
    {
    $klubs = DB::table('klubs')->paginate(10);

    return view('klub', ['klubs' => $klubs]);
    }
    public function create()
    {
        return view('klub/create', get_defined_vars());
    }
    public function store(Request $request)
    {
        $messages = [

            'required'=> ':attribute tidak boleh kosong',
            'unique'=> ': data tidak boleh sama',
            'max'=> ':attribute maximal :max karakter',
            'min'=> ':attribute minimal :min karakter'

        ];
        $request->validate([
            'nama_klub' => 'required|min:5|max:20|unique:klubs,nama_klub',
            'kota_klub' => 'required|min:5',
        ],$messages);
        //        dd($request->all());
        DB::table('klubs')->insert([
            'nama_klub' => $request->nama_klub,
            'kota_klub' => $request->kota_klub
        ]);

        $main=0;
        $menang=0;
        $seri=0;
        $kalah=0;
        $golmasuk=0;
        $golkemasukan=0;
        $point=0;
        DB::table('klasemens')->insert([
            'nama_klub' => $request->nama_klub,
            'main' => $main,
            'menang' => $menang,
            'seri' => $seri,
            'kalah'=> $kalah,
            'golmasuk'=> $golmasuk,
            'golkemasukan'=>$golkemasukan,
            'point'=>$point

        ]);
        
        Session::flash('sukses', 'data disimpan telah disimpan');
        return redirect('web/klub');
        // alihkan halaman ke halaman klub
    }
}