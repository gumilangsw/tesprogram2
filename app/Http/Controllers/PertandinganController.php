<?php
 
 namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


use Session;
class PertandinganController extends Controller
{
    public function pertandingan()
    {
    $klubs = DB::table('klubs')->get();
    return view('pertandingan',['klubs' => $klubs]);
    }
    public function store(Request $request)
    {
        $messages = [

            'required'=> ':attribute tidak boleh kosong',
            'unique'=> ': data tidak boleh sama',
            'max'=> ':attribute maximal :max karakter',
            'min'=> ':attribute minimal :min karakter'

        ];
        $request->validate([
            'nama_klub_kandang' => 'required|min:5|max:20|unique:pertandingans,nama_klub_kandang',
            'nama_klub_tandang' => 'required|min:5|unique:pertandingans,nama_klub_tandang',
        ],$messages);
        //        dd($request->all());
        DB::table('pertandingans')->insert([
            'nama_klub_kandang' => $request->nama_klub_kandang,
            'klub_kandang_id' => $request->klub_kandang_id,
            'nama_klub_tandang' => $request->nama_klub_tandang,
            'klub_tandang_id' => $request->klub_tandang_id,
            'skor_klub_kandang' => $request->skor_klub_kandang,
            'skor_klub_tandang' => $request->skor_klub_tandang,
            'status' => $request->status
        ]);
        
        Session::flash('sukses', 'data disimpan telah disimpan');
        return redirect('web/pertandingan');
        // alihkan halaman ke halaman klub
    }
}