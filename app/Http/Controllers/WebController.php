<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
 
class WebController extends Controller
{

	public function home(){
		return view('home');
	}
 
	public function klub(){
		return view('klub');
	}
 
	public function klasemen(){
		return view('klasemen');
	}
 
}