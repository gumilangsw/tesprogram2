<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('klasemens', function (Blueprint $table) {
            $table->id();
            $table->text('nama_klub');
            $table->text('main');
            $table->text('menang');
            $table->text('seri');
            $table->text('kalah');
            $table->text('golmasuk');
            $table->text('golkemasukan');
            $table->text('point');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('klasemens');
    }
};
