<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pertandingans', function (Blueprint $table) {
            $table->id();
            $table->text('nama_klub_kandang');
            $table->text('klub_kandang_id');
            $table->text('nama_klub_tandang');
            $table->text('klub_tandang_id');
            $table->text('skor_klub_kandang');
            $table->text('skor_klub_tandang');
            $table->text('status');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pertandingans');
    }
};
