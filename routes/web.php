<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WebController;
use App\Http\Controllers\KlubController;
use App\Http\Controllers\KlasemenController;
use App\Http\Controllers\PertandinganController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
// route blog
Route::get('/', [WebController::class, 'home'])->name('home');
//Route::get('/web/klub', [WebController::class, 'klub'])->name('klub');
//Route::get('/web/klasemen', [WebController::class, 'klasemen'])->name('klasemen');

//input klub
Route::get('/web/klub', [KlubController::class, 'klub'])->name('klub');
Route::get('/klub/create', [KlubController::class, 'create'])->name('klub.create');
Route::post('/klub/store', [KlubController::class, 'store']);

//klasemen
Route::get('/web/klasemen', [KlasemenController::class, 'klasemen'])->name('klasemen');


//pertandingan
Route::get('/web/pertandingan', [PertandinganController::class, 'pertandingan'])->name('pertandingan');
Route::post('/pertandingan/store', [PertandinganController::class, 'store']);

        